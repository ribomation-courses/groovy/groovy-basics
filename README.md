Groovy Basics
====

Welcome to this course.
The syllabus can be find at
[groovy/groovy-basics](https://www.ribomation.se/courses/groovy/groovy.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Demo programs

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    mkdir -p ~/groovy-course/my-solutions
    cd ~/groovy-course
    git clone https://gitlab.com/ribomation-courses/groovy/groovy-basics.git gitlab
    cd gitlab

Get the latest updates by a `git pull` operation

    cd ~/groovy-course/gitlab
    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have
the following installed. 

* [Java 8 JDK, or later](https://adoptopenjdk.net/)
* [Groovy](http://groovy-lang.org/download.html)
* [Gradle](https://gradle.org/install/)

If you are running a BASH shell (e.g. *GIT BASH Client*), then go for [SDKMAN](http://sdkman.io/index.html), 
which can download and install all tools above.

    sdk install java 8.0.232.hs-adpt
    sdk install groovy 3.0.0-rc-2
    sdk install gradle 6.0.1

In addition, you need to be using a decent Java IDE, such as

* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download) 
* [MS Visual Code](https://code.visualstudio.com/Download)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
