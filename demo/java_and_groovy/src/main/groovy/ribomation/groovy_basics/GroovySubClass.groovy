package ribomation.groovy_basics

import groovy.transform.*

@ToString(includeNames = true, includePackage = false, includeSuper = true)
@EqualsAndHashCode(callSuper = true)
@AutoClone(style = AutoCloneStyle.SERIALIZATION)
class GroovySubClass extends JavaBaseClass {
    String msg ='no msg';
    float num = -1;

    GroovySubClass(String name, int number) {
        super(name, number)
    }

    GroovySubClass(String name, int number, String msg, float num) {
        super(name, number)
        this.msg = msg
        this.num = num
    }

    GroovySubClass mkModif() {
        def copy    = this.clone()
        copy.name   = "${name} ${name}son".split(/\s+/)*.capitalize().join(' ')
        copy.number = copy.number**3
        copy
    }
}


