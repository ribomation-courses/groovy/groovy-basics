package ribomation.groovy_basics;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaMainApp {
    public static void main(String[] args) {
        JavaMainApp app = new JavaMainApp();
        app.run();
    }

    void run() {
        GroovySubClass obj = mk();
        System.out.printf("obj: %s%n", obj);

        obj = modify(obj);
        System.out.printf("obj: %s%n", obj);

        use(obj);
        System.out.printf("obj: %s%n", obj);

        GroovySubClass obj2 = obj.mkModif();
        System.out.printf("obj2: %s%n", obj2);

        lst();
    }

    GroovySubClass mk() {
        return new GroovySubClass(
                "nisse", 42,
                "tjolla hopp", 3.1415F);
    }

    GroovySubClass modify(GroovySubClass obj) {
        obj.setName("anna");
        obj.setNumber(17);
        obj.setMsg("hipp happ");
        obj.setNum(2.71F);
        return obj;
    }

    void use(JavaBaseClass obj) {
        System.out.printf("obj: %s%n", obj);
        obj.setName("olle");
    }

    void lst() {
        List<GroovySubClass> objs = Stream.of("anna", "berit", "carin", "diana")
                .map(name -> new GroovySubClass(name, name.length() * 10))
                .peek(System.out::println)
                .map(GroovySubClass::mkModif)
                .collect(Collectors.toList());
        objs.forEach(System.out::println);
    }
}
