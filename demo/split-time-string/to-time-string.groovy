String toTimeString(txt, sep) {
    def tokens = txt.split(sep) as ArrayList
    def units  = ['h', 'm', 's']
    [tokens, units]
    .transpose()
    .collect { it.join() }
    .join(' ')
}

def input = '10:20:35'
def delim = ':'
println "Time: ${toTimeString(input, delim)}"
