@groovy.transform.Field
def javaSpec = 'https://docs.oracle.com/javase/specs/'

@Grab('org.ccil.cowan.tagsoup:tagsoup:1.2.1')
def getHtml() {
    def parser = new XmlParser(new org.ccil.cowan.tagsoup.Parser())
    parser.parse(javaSpec)
}
html.body.'**'.a.@href
    .grep(~/.*\.pdf/)
    .collect{new URL(new URL(javaSpec), it)}
    .each{ println it }
''