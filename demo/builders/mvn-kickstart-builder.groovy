def pomUrl = 'https://gist.githubusercontent.com/torgeir/6742158/raw/249cf3b968e0ea3f8bfa5ae5157a5d747af00b7c/mininal-maven-pom.xml'
def pomXml = pomUrl.toURL().text

def projDir = new File('./mvn-kickstart')
"rm -rf ${projDir}".execute().err.text
projDir.mkdirs()

def proj = new FileTreeBuilder(projDir)
proj {
  'pom.xml'(pomXml)
  src {
    dir('main') {
     java {
       ribomation {
         app {
           'App.java'('''package ribomation.app;
           public class App {
             public static void main(String[] args) {
               System.out.printf("Hi there%n");
             }
           }
           '''.stripIndent())
         }
       }
     }
    }
    resources {
      file('system.properties') {
        withOutputStream {out -> System.properties.store(out, 'sys props')}
      }
    }
  }  
}

println "created maven project: ${projDir}"

