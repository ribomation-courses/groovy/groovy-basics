import groovy.swing.SwingBuilder
import java.awt.BorderLayout as BL
import javax.swing.WindowConstants as WC
import javax.swing.SwingConstants as SC
import javax.swing.JOptionPane as OPT
import java.awt.FlowLayout as FL

new SwingBuilder()
.frame(
        title: 'Swing Demo', 
        size: [300,200], 
        show: true, 
        locationRelativeTo: null,
        defaultCloseOperation: WC.EXIT_ON_CLOSE
    ) {
    lookAndFeel('system')
    menuBar() {
        menu(text: 'File') {
            menuItem(text: 'Reset', actionPerformed: {
                count = 0
                message.text = 'Click the Button'
            })
            menuItem(text: 'Quit', actionPerformed: {
                dispose()
            })
        }
        menu(text: 'Help') {
            menuItem(text: 'About', actionPerformed: { 
                OPT.showMessageDialog(null, 'Simple Swing Demo')
            })
        }
    }
    borderLayout()
    count = 0
    message = label(text:'Click the Button!', horizontalAlignment: SC.CENTER, constraints:BL.CENTER)
    button(
        text: 'Click Me', 
        constraints: BL.SOUTH,
        actionPerformed: {
            ++count
            message.text = "You clicked ${count} times"
        }
    )
}
''
