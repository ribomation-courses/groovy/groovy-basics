class Product {
    String  name         = 'no name'
    float   price        = 100 + (Math.random()*500) as int
    int     itemsInStock = (Math.random()*100) as int
    Date    date         = new Date() + ((Math.random()*45) as int)
    String  toString() {"""
Product{
  name : ${name}
  price: �${price}
  count: ${itemsInStock}
  date : ${date.format('d MMM yyyy')}
}"""}
}

def products = [
    new Product(name:'Groovy Primer'),
    new Product(name:'Java Kickstart'),
    new Product(name:'Scala Tutorial'),
    new Product(name:'Kotlin Up & Running'),
]
products << new Product(name:'Clojure Book')
for (p in products) print p
println ''

println '-' * 30
def N    = 10_000
def nums = (1..N)
println "SUM(1..$N) = ${nums.sum()}"
println '-' * 30

for (e in System.properties)
  if (e.key =~ /.+vm.+/)
    println "${e.key}: ${e.value}" 
