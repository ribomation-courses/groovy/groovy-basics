import groovy.json.JsonSlurper
import java.util.zip.GZIPInputStream as GZIP

def mostPopular(url, max=15) {
    def input = new GZIP( url.toURL().newInputStream() )
    new JsonSlurper().parse(input)
        .items
        .take(max)
        .withIndex()
        .collect {node, row ->
           String.format '%02d) %-20s: %d', row+1, node.name, node.count
        }
        .join('\n')
}

def url = 'https://api.stackexchange.com/2.2/tags?order=desc&max=9999&sort=popular&site=stackoverflow'
println  mostPopular(url, args ? args[0].toInteger() : 12)

''