class Product {
    String name         = 'no name'
    float  price        = 25
    int    itemsInStock = 0
    Date   date         = new Date()
    String toString() {
        "Product{name=${name}, price=�${price}" +
        ", count=${itemsInStock}, date=${date.format('yyyy-MM-dd HH:mm')}}"
    }
}

def p = new Product(itemsInStock:10, name:'Groovy Book')
println p
p.with {
    name          = name.toUpperCase()
    price        *= 3
    itemsInStock -= 6
}
println p
