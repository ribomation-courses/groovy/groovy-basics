java.lang.String.metaClass.toSnakeCase = {->
    delegate.split(/\s+/)*.capitalize().join('_')
}

def s = 'foobar strikes again'
println "${s} -> ${s.toSnakeCase()}"

// -------------
java.util.Date.metaClass.getAsISO8601 = {->
    delegate.format('yyyy-MM-dd HH:mm:ss')
}
def d = new Date()
println "${d} -> ${d.asISO8601}"