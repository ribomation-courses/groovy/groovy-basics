@groovy.transform.ToString(includeNames=true)
class Product { String name; float price; int itemsInStock; Date date }

def r = new Random()
def products = (1..100).collect {id ->
    new Product(name:         "product-$id", 
                price:        100 + r.nextInt(900),
                itemsInStock: r.nextInt(15),
                date:         new Date() -  r.nextInt(30)
                )
}

def SEP(String msg) { println "${'=' * 45}\n--- ${msg} ---" }

SEP('lowest in price=[500, 700]')
println products
            .findAll {500 <= it.price && it.price <= 700}
            .min { it.price }
            
SEP('avg. price for items out of stock')
def lst = products.grep {it.itemsInStock == 0}
printf 'result: %.2f kr%n', lst*.price.sum() / lst.size()

SEP('items grouped by weekday')
products
    .groupBy {it.date.toString()[0..2]}
    .sort {lhs,rhs -> lhs.key <=> rhs.key}
    .each {day,prods -> println "${day}: ${prods.take(3)*.name}"}

''