import static groovy.io.FileType.*
def dir = ('..\\' * 5) as File
println dir.canonicalPath

def files = []
dir.eachFileRecurse(FILES) {files << it}

files.grep {it.name.endsWith('groovy') || it.name.endsWith('java') }
     .collect { it.text.split(/\W+/).grep {it.size() > 2} }
    .flatten()
    .countBy {it}
    .sort {lhs,rhs -> rhs.value <=> lhs.value}
    .take(10)
    .each {word, freq -> printf "%s: %d%n", word.padRight(10,' '), freq}
''