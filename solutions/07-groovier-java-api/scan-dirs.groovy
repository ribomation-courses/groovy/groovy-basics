import static groovy.io.FileType.*
def dir = ('..\\' * 4) as File
println dir.canonicalPath

def words = []
dir.eachFileRecurse(FILES) {f ->
    def ext = f.name.substring(f.name.lastIndexOf('.') + 1)
    if (ext in ['groovy', 'java']) { 
         words << f.text.split(/\W+/).grep {it.size() > 1}
    }
}

words.flatten()
     .countBy {it}
     .sort {lhs,rhs -> rhs.value <=> lhs.value}
     .take(10)
     .each {word, freq ->
         println "${word.padRight(10,' ')}: ${freq}"
     }
''