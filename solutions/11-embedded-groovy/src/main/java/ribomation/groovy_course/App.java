package ribomation.groovy_course;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

public class App {
    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    void run(String[] args) throws Exception {
        for (String arg : args) {
            System.out.printf("%s -> %s%n", arg, doStuffInGroovy(arg));
        }
    }

    final String SCRIPT = "arg.capitalize().center(30,'*')";
    final Script script = new GroovyShell().parse(SCRIPT);

    String doStuffInGroovy(String arg) {
        Binding vars = new Binding();
        vars.setVariable("arg", arg);
        script.setBinding(vars);
        return (String) script.run();
    }

}
