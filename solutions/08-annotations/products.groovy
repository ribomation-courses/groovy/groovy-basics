package ribomation.groovy_basics
import groovy.transform.*

@Immutable()
@Sortable(includes = ['name', 'price'])
@EqualsAndHashCode(includes = ['name', 'price'])
@ToString(includeNames=true, includePackage=false, ignoreNulls=true)
class Product {
    String   name
    Float    price
    Integer  itemsInStock
    Date     date
}

@Singleton()
@ToString(includeNames=true, includePackage=false, ignoreNulls=true)
class ProductRegistry {
    private final Random  r = new Random()
    private final names = [
        'Groovy', 'Java', 'Scala', 'Clojure', 
        'JavaScript', 'TypeScript', 'Groovy', 
        'Java', 'Scala', 'Groovy', 'Java', 'Perl'
    ]
    TreeSet products = []
    
    void generate() {
        def p =  new Product(name        : names[r.nextInt(names.size())] + ' Course', 
                             price       : 100 + r.nextInt(500), 
                             itemsInStock: r.nextInt(12),
                             date        : new Date() -  r.nextInt(45))
        products << p
    }
}

100.times { name -> ProductRegistry.instance.generate() }

ProductRegistry.instance.products.each {println it}
''