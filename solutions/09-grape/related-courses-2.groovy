@Grab('org.ccil.cowan.tagsoup:tagsoup:1.2.1')
def dummy

new XmlParser(new org.ccil.cowan.tagsoup.Parser())
    .parse('https://www.ribomation.se/build/gulpjs.html')
    .body.'**'.td.a
    .findAll {it['@class'] == 'hoverable'}
    *.text()*.trim()
    .eachWithIndex {name, idx -> println "${idx+1}) ${name}"}
''