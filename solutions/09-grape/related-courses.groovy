@groovy.transform.Field
def url = 'https://www.ribomation.se/groovy/groovy-basics.html'
//def url = 'https://www.ribomation.se/cxx/cxx-14.html'

@Grab('org.ccil.cowan.tagsoup:tagsoup:1.2.1')
def getHtml() {
    def parser = new XmlParser(new org.ccil.cowan.tagsoup.Parser())
    parser.parse(url)
}

html.body.'**'.td.a
    .findAll {it['@class'] == 'hoverable'}
    *.text()*.trim()
    .eachWithIndex {name, idx -> println "${idx+1}) ${name}"}

''