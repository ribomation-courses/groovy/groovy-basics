@Grapes(
    @Grab(group='org.jeasy', module='easy-random-core', version='4.1.0')
)
import org.jeasy.random.EasyRandom
import org.jeasy.random.EasyRandomParameters
import groovy.transform.*

@ToString(includeNames=true, ignoreNulls=true)
@Sortable
class Person {
    String    name
    Integer   age
    Date      date
}

final String[] names = ['Anna', 'Berit', 'Carin', 'Doris']
final Random r=new Random()

def params = new EasyRandomParameters()
   .randomize(String.class, { names[r.nextInt(names.size())] }  )
   .randomize(Integer.class, { 20 + r.nextInt(50) }  )
   .randomize(Date.class, { new Date() - r.nextInt(365) }  )
   .ignoreRandomizationErrors(true);
   
def gen = new EasyRandom(params)
TreeSet persons = (1..25).collect {gen.nextObject(Person.class)}

persons.each {println it}

''