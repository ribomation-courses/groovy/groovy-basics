@groovy.transform.ToString(includeNames=true)
class Product {
    String  name         = ''
    float   price        = 10 + (Math.random()*200 as int)
    int     itemsInStock = Math.random()*100 as int
    Date    date         = new Date() - (Math.random()*45 as int)
    Product next()     {++itemsInStock; this}
    Product previous() {--itemsInStock; this}
}

def names = ['java', 'groovy', 'scala', 'clojure', 'kotlin', 
             'jruby', 'jython', 'jaskel', 'erjang']
def products = names.collect { new Product(name:it.toUpperCase() + ' Book') }

def avgPrice = products*.price.sum() / products.size()
printf 'AVG. price = %.2f kr%n', avgPrice

println ('=' * 30)
println products.sort {lhs, rhs -> rhs.price <=> lhs.price} .join('\n')

println '-' * 30
println (++products[-2])
println (--products[-1])

