import groovy.transform.*

@ToString(includeNames=true)
class Customer {
    String  name
    List    invoices = []
}

@ToString(includeNames=true)
class Invoice {
    Date    order
    List    products = []
}

@ToString(includeNames=true)
class Product {
    String  name
    float   price
}

def objs = new ObjectGraphBuilder()
objs.classLoader = getClass().classLoader

def langs = ['groovy','java','c++','go']
def c = objs.
customer(name:'Justin Time') {
  10.times {
      invoice(order: new Date() - new Random().nextInt(123)) {
        langs.collect {
          product name:"${it.capitalize()} book", price:100+new Random().nextInt(500)
        }
      }     
  }
}

println "Customer: ${c.name}\n" +
c.invoices.sort {a,b -> a.order <=> b.order}.collect {
    "* Order: ${it.order.format('yyyy-MM-dd')}\n  " + 
    it.products.sort {a,b -> a.price <=> b.price}.join('\n  ')
}.join('\n')
