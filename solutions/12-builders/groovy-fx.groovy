@Grapes([
    @Grab(group='org.openjfx', module='javafx-base', version='13.0.2'),
    @Grab(group='org.openjfx', module='javafx-controls', version='13.0.2'),
     @Grab(group='org.openjfx', module='javafx-graphics', version='13.0.2'),
     @Grab(group='org.jfxtras', module='jfxtras-all', version='9.0-r1'),
    @Grab('org.groovyfx:groovyfx:8.0.0')
])
import static groovyx.javafx.GroovyFX.start
import java.util.concurrent.CountDownLatch
import javafx.embed.swing.JFXPanel
import javax.swing.SwingUtilities

final CountDownLatch latch = new CountDownLatch(1)
SwingUtilities.invokeLater({new JFXPanel(); latch.countDown()} as Runnable)
latch.await()

start {
    stage(title: 'GroovyFX Hello World', visible: true) {
        scene(fill: BLACK, width: 500, height: 250) {
            hbox(padding: 60) {
                text(text: 'Groovy', font: '80pt sanserif') {
                    fill linearGradient(endX: 0, stops: [PALEGREEN, SEAGREEN])
                }
                text(text: 'FX', font: '80pt sanserif') {
                    fill linearGradient(endX: 0, stops: [CYAN, DODGERBLUE])
                    effect dropShadow(color: DODGERBLUE, radius: 25, spread: 0.25)
                }
            }
        }
    }
}