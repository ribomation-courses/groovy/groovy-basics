#!/usr/bin/env groovy
import static groovy.io.FileType.*

def scanDir(File dir, int max=10) {
    def EXTs  = ['groovy', 'java']
    def words = []
    dir.eachFileRecurse(FILES) {f ->
        def ext = f.name.substring(f.name.lastIndexOf('.')+1)
        if (ext in EXTs) { 
             words << f.text.split(/\W+/).grep {it.size() > 1}
        }
    }
    words.flatten()
        .countBy {it}
        .sort {lhs,rhs -> rhs.value <=> lhs.value}
        .take(max)
}

long start = System.nanoTime()
args = args ?: '.'
args.each {dir -> 
    dir = dir as File  // new File(dir)
    println "DIR: ${dir.canonicalPath}"
    scanDir(dir).each {word,freq ->
        println "${word.padLeft(12,' ')}: ${freq}"
    }
}
long elapsed = System.nanoTime() - start
println "elapsed: ${elapsed * 1E-9} seconds"
