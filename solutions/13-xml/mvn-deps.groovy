def url = 'https://repo1.maven.org/maven2/org/springframework/spring/2.5.6/spring-2.5.6.pom'
def pom = new XmlParser().parse(url)

pom.dependencies.dependency
    .eachWithIndex {dep, ix ->
        def group   = dep.groupId.text()
        def module  = dep.artifactId.text()
        def version = dep.version.text()
        println "${ix+1}) " + [group, module, version].join(':')
    }

println 'license : ' + pom.licenses.license.url.text()
println 'vers ctr: ' + pom.scm.url.text()
println 'mvn repo: ' + pom.distributionManagement.snapshotRepository.url.text()
''