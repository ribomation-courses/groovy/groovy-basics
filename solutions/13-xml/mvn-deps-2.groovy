def url = 'https://repo1.maven.org/maven2/org/springframework/spring/2.5.6/spring-2.5.6.pom'
def pom = new XmlSlurper().parse(url)

pom.dependencies.dependency
   .eachWithIndex {dep, row ->
       def payload = [
           dep.groupId.text(),
           dep.artifactId.text(),
           dep.version.text()
       ]
       println "${row + 1}) " + payload.join(':')
}
''
